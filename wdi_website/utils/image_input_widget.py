from django.forms import ClearableFileInput


class ImageInput(ClearableFileInput):
    """
    This is exactly the same widget as the ClearableFileInput, except it uses an adjusted template.
    """
    template_name = "widgets/image_input.html"
