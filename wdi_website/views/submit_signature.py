import logging
import uuid

from django.conf import settings
from django.contrib import messages
from django.core.exceptions import PermissionDenied
from django.core.mail import send_mail
from django.http import HttpResponseRedirect
from django.template.loader import render_to_string
from django.utils.translation import gettext as _, get_language_from_request, override
from django.views.decorators.http import require_GET, require_POST
from ratelimit.decorators import ratelimit

from wagtail.models import Locale

from wdi_website.forms import SignatureWithOrganizationForm
from wdi_website.models.models import Signature, SignatureConfirmationToken
from wdi_website.models.pages import StandardPage


# Get an instance of a logger
logger = logging.getLogger(__name__)


def confirmation_email_plain_text_message(token):
    message = (
        _("Thank you for signing the Declaration!")
        + " "
        + _(
            "Please use this link to confirm your signature and let it appear on the public Signatories list:"
        )
        + " "
        + settings.WAGTAILADMIN_BASE_URL
        + "/verify/"
        + token
        + "\n\n"
        + _(
            "We would like to thank you for helping to protect sex-based rights of women and girls. The change starts "
            "with women and allies just like you. Your support means the world to us. Thank you!"
        )
        + "\n\n"
        + _(
            "If the button above doesn't work, please let us know by replying to this e-mail."
        )
        + "\n"
        + _(
            "If you did not sign the Declaration on Women's Sex-Based rights, please disregard this message."
        )
        + "\n\n"
        + _(
            "Women's Declaration International is a group of volunteer women who are dedicated to preserving our "
            "sex-based rights. Join us in defending women and girls."
        )
        + "\n\nWomen's Declaration International, Suite A, 82 James Carter Rd Mildenhall, Suffolk, IP28 7DE, UK. www.womensdeclaration.com"
    )

    return message


def org_signup_plain_text_message(org_name):
    message = (
        _("Organization")
        + " "
        + org_name
        + " "
        + _("has signed up the declaration.")
        + " "
        + _(
            "Please keep in mind that anyone might try to sign the Declaration in the name of a certain organization. "
            "Each signature should be confirmed by the said organization and carefully reviewed by the WDI team before "
            "being accepted as genuine. For this same reason, proceed with caution when following any links provided on a signature."
        )
        + "\n\n"
        + _(
            "If you have any concerns that an organization signature might be false, please contact info@womensdeclaration.com."
        )
        + "\n\n"
        + _(
            "Women's Declaration International is a group of volunteer women who are dedicated to preserving our "
            "sex-based rights. Join us in defending women and girls."
        )
        + "\n\nWomen's Declaration International, Suite A, 82 James Carter Rd Mildenhall, Suffolk, IP28 7DE, UK. www.womensdeclaration.com"
    )

    return message


def send_new_token_for_signature(signature):
    """
    Retrieves an existing confirmation token or generates a new one and sends an email with a confirmation
    link to a person with an unconfirmed signature.
    :param signature: A signature to confirm
    :return: True if the email has been successfully sent, False if error occurred.
    """

    # Verify if a confirmation token already exists for that signature and if it doesn't, create a new one
    try:
        signature_token = SignatureConfirmationToken.objects.get(
            signature_id=signature.id
        )
    except SignatureConfirmationToken.DoesNotExist:
        signature_token = SignatureConfirmationToken(
            token=uuid.uuid4().hex, signature=signature
        )
        signature_token.save()

    sent_email_successfully = 0
    for i in range(0, 3):
        try:
            sent_email_successfully = send_mail(
                _(
                    "Thank you for signing the Declaration! Please confirm your signature."
                ),
                confirmation_email_plain_text_message(signature_token.token),
                settings.FROM_EMAIL_ADDRESS,
                [signature.email],
                html_message=render_to_string(
                    "wdi_website/confirmation_email.html",
                    {"token": signature_token.token, "url": settings.WAGTAILADMIN_BASE_URL},
                ),
                fail_silently=False,
            )
            if sent_email_successfully > 0:
                return True
        except Exception as e:
            logger.exception(
                "Failed to send the confirmation email for the {} time".format(i + 1)
            )
            continue

    if sent_email_successfully == 0:
        signature_token.delete()  # If we could not send the email, delete the confirmation token
        return False

    return sent_email_successfully > 0


@ratelimit(key="ip", rate="100/h")
def resend_confirmation_email(request, email):
    """
    Retrieves an existing signature and resends a confirmation email to a person with an unconfirmed signature.
    :param email: An email address
    :return: (HttpResponseRedirect) A Http Redirect to the admin signature page.
    """

    # Only allow those with View Signature permissions to resend a confirmation email
    if not request.user.has_perm("wdi_website.view_signature"):
        raise PermissionDenied

    try:
        signature = Signature.objects.get(email=email)
    except Signature.DoesNotExist:
        messages.error(
            request,
            _(
                "Signature associated with email {email} not found.".format(
                    email=email
                )
            ),
        )
        return HttpResponseRedirect("/admin/wdi_website/signature/")

    if signature.email_confirmed:
        messages.error(
            request,
            _(
                "Signature by {name} ({email}) has already been confirmed.".format(
                    name=signature.name, email=signature.email
                )
            ),
        )
        return HttpResponseRedirect("/admin/wdi_website/signature/")

    letter_language = signature.language if signature.language else "en"
    with override(letter_language, deactivate=True):
        email_sent = send_new_token_for_signature(signature)

    if email_sent:
        messages.success(
            request,
            _(
                "Confirmation email to {name} ({email}) sent successfully.".format(
                    name=signature.name, email=signature.email
                )
            ),
        )
    else:
        messages.error(
            request,
            _(
                "Failed to send the confirmation email to {name} ({email}).".format(
                    name=signature.name, email=signature.email
                )
            ),
        )

    return HttpResponseRedirect("/admin/wdi_website/signature/")


def send_email_organization_signup(signature, org):
    """
    Sends an email to admins when a new organization signs up.
    :param signature: An organization signature
    :return: True if the email has been successfully sent, False if error occurred.
    """

    recipient_address = [settings.TO_ADDRESS_ORG_REGISTERED]

    sent_email_successfully = 0
    for i in range(0, 3):
        try:
            sent_email_successfully = send_mail(
                _("New organization sign-up: {}".format(signature.organization)),
                org_signup_plain_text_message(signature.organization),
                settings.FROM_EMAIL_ADDRESS,
                recipient_address,
                html_message=render_to_string(
                    "wdi_website/org_signup_email.html",
                    {
                        "org_name": signature.organization,
                        "org_id": org.id,
                        "url": settings.WAGTAILADMIN_BASE_URL,
                    },
                ),
                fail_silently=False,
            )
            if sent_email_successfully > 0:
                return True
        except Exception as e:
            logger.exception(
                "Failed to send the organization sign-up email for the {} time".format(
                    i + 1
                )
            )
            continue

    if sent_email_successfully == 0:
        return False

    return sent_email_successfully > 0


@require_POST
@ratelimit(key="ip", rate="100/h")
def submit_signature(request):
    # create a form instance and populate it with data from the request:
    form = SignatureWithOrganizationForm(request.POST, request.FILES)

    user_language = get_language_from_request(request, check_path=True)

    # check whether the form is valid:
    if form.is_valid():
        signature, org = form.save_with_language(user_language)  # Save the form
        message_sent = send_new_token_for_signature(signature)

        if signature.sign_for_organization:
            org_message_sent = send_email_organization_signup(signature, org)

        if not message_sent or (
            signature.sign_for_organization and not org_message_sent
        ):  # Could not send a message - delete the objects and ask the user to retry
            signature.delete()
            if org is not None:
                org.delete()

            return set_error_retry_response(
                request,
                form,
                user_language,
                message=_("There was an error processing your form. Please try again."),
            )

        # Set a success message
        messages.success(
            request,
            _(
                "Thank you for signing the Declaration! Please check your inbox and confirm your signature!"
            ),
        )

        # Delete saved form data
        if "sign_declaration_form_data" in request.session:
            del request.session["sign_declaration_form_data"]

        return HttpResponseRedirect("/")

    elif form.errors:
        # This email already exists in the database
        if "email" in form.errors and "Duplicate email" in form.errors["email"]:

            # Set an error message
            messages.error(request, _("You have already signed the Declaration!"))

            # Delete saved form data
            if "sign_declaration_form_data" in request.session:
                del request.session["sign_declaration_form_data"]

            return HttpResponseRedirect("/")

        # Incorrect captcha
        elif "captcha" in form.errors:
            return set_error_retry_response(
                request,
                form,
                user_language,
                message=_("Incorrect Captcha solution. Please try again."),
            )

        # Any other error
        else:
            # Log the error
            logger.error(
                "Signature form error! JSON data:" + str(form.errors.get_json_data())
            )
            form_error_messages = []
            for field in form.errors:
                for errors_for_field in form.errors[field]:
                    form_error_messages.append(errors_for_field)

            if not form_error_messages:
                form_errors_message_text = _(
                    "There is something wrong with your form. Please check that all the values are correct and try again."
                )
            else:
                form_errors_message_text = "\n".join(
                    [_(e) for e in form_error_messages]
                )

            return set_error_retry_response(
                request,
                form,
                user_language,
                message=form_errors_message_text,
            )

    else:  # The form is invalid, but no form errors are present. Something went wrong
        return set_error_retry_response(
            request,
            form,
            user_language,
            message=_("There was an error processing your form. Please try again."),
        )


def set_error_retry_response(request, form, user_language, message):
    """
    Sets an error message and pre-fills the form so that the user can re-try the submission.
    :param request: A user's request
    :param form: A form that the user sent
    :param user_language: (str) a language code for the language of the website that the user uses.
    :param message: A translated string. The text of the message to be displayed.
    :return: (HttpResponseRedirect) A Http Redirect to the signature form.
    """
    # Set an error message
    messages.error(request, message)

    # Remember form data and save it in session for a form pre-fill
    request.session["sign_declaration_form_data"] = form.data

    # Redirect straight to the signature form
    return HttpResponseRedirect("/{}/#sign".format(user_language))


@ratelimit(key="ip", rate="100/h")
@require_GET
def verify_signature(request, input_token):
    token = SignatureConfirmationToken.objects.filter(token=input_token).first()
    if token is None or token.signature is None:
        messages.error(
            request,
            _(
                "The confirmation code you've tried to use is invalid. Please use the confirmation code you've "
                "received in an e-mail."
            ),
        )
        return HttpResponseRedirect("/")

    # The signature and the token are all good - confirm the signature
    signature = token.signature
    if signature.email_confirmed is False:
        signature.email_confirmed = True
        signature.save()
        return get_redirect_to_thank_you(request, signature.language)

    # Trying to confirm a signature that has already been confirmed
    else:
        messages.error(request, _("You have already confirmed your signature before."))

    return HttpResponseRedirect("/")


def get_redirect_to_thank_you(request, language):
    try:
        locale = Locale.objects.get(language_code=language)
        thank_you_page = (
            StandardPage.objects.filter(slug="thank-you", live=True, locale=locale)
            | StandardPage.objects.filter(
                 slug="thank-you-" + language, live=True, locale=locale
            )
        ).get()

        return HttpResponseRedirect(thank_you_page.get_url())

    except (StandardPage.DoesNotExist, Locale.DoesNotExist):
        messages.success(
            request,
            _(
                "Thank you! You have confirmed your signature. It will now be visible on the signatories page."
            ),
        )
        return HttpResponseRedirect("/")
