from django.core.management.base import BaseCommand
import requests
from requests.structures import CaseInsensitiveDict

from wdi_website.models.models import Organization


class Command(BaseCommand):
    help = 'Unconfirms organizations whose url is invalid'

    def handle(self, *args, **options):
        orgs = Organization.objects.filter(confirmed=True).all()

        num_orgs = len(orgs)
        num_unconfirmed = 0

        for o in orgs:
            if len(o.homepage) == 0:
                continue # We keep orgs without URLs in place, since we can't check validity of nonexisting URLs
            try:
                org_homepage_request = self.request_url(o.homepage)
                if org_homepage_request.status_code != 200:
                    self.unconfirm_org(o)
                    num_unconfirmed += 1

            except requests.exceptions.RequestException:
                self.unconfirm_org(o)
                num_unconfirmed += 1

        self.stdout.write(self.style.SUCCESS(f"Checked {num_orgs} organisations, un-confirmed {num_unconfirmed}"))

    def unconfirm_org(self, org):
        self.stdout.write(
            f"Organization {org.name} has invalid homepage ({org.homepage}) and has been un-confirmed.")
        org.confirmed = False
        org.save()

    def request_url(self, url):
        """
        Request a URL while pretending to be Mozilla Firefox browser. The camouflaging is needed because some sites
        do not allow clients with non-standard user agent to access the site. This might result in many false
        negatives.
        """
        headers = CaseInsensitiveDict()
        headers["Connection"] = "keep-alive"
        headers["User-Agent"] = "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:71.0) Gecko/20100101 Firefox/71.0"
        headers["Upgrade-Insecure-Requests"] = "1"
        headers["Accept"] = "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8"
        headers["Accept-Language"] = "en-US,en;q=0.5"
        headers["Accept-Encoding"] = "gzip, deflate"

        resp = requests.get(url, headers=headers)
        return resp
