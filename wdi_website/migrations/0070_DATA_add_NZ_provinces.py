from django.db import migrations, models
from iso3166_2 import ISO3166_2

old_to_new_mapping = {
    "NL-CAN": ["Ashburton", "Ellesmere"],
    "NZ-AUK": ["Bay of Islands", "Rodney"],
    "NZ-WGN": ["Featherston", "Hutt"],
    "NZ-TAS": ["Golden Bay"],
    "NZ-HKB": ["Hawke's Bay"],
    "NZ-MWT": ["Manawatu", "Horowhenua"],
    "NZ-MBH": ["Marlborough"],
    "NZ-STL": ["Southland"],
    "NZ-TKI": ["Taranaki"],
    "NL-BOP": ["Tauranga"],
    "NZ-WKO": ["Waikato"],
    "NL-NTL": ["Whangarei"],
}


def fix_nz_provinces(apps, schema_editor):
    Province = apps.get_model("wdi_website", "Province")
    Signature = apps.get_model("wdi_website", "Signature")
    iso = ISO3166_2()
    new_provinces = iso["NZ"]

    for new_province_code in new_provinces:

        # Save a new province
        new_province = iso["NZ"][new_province_code]
        p = Province(name=new_province["name"], country="NZ",
                     iso3166_2_code=new_province_code)
        p.save()

        if new_province_code in old_to_new_mapping:
            for old_province_name in old_to_new_mapping[new_province_code]:
                old_province = Province.objects.filter(name=old_province_name, country="NZ", iso3166_2_code="-").get()
                signatures_in_old_province = Signature.objects.filter(province=old_province).all()

                # Reassign signatures to a new province
                for s in signatures_in_old_province:
                    s.province = p
                    s.save()


def delete_all_unclaimed_provinces(apps, schema_editor):
    Province = apps.get_model("wdi_website", "Province")

    # This will make sure that old provinces won't show up in form, while preserving the foreign key on Signature model
    # Only "unclaimed" subdivisions that have a default ISO code are affected
    Province.objects.filter(iso3166_2_code="-", country="NZ").update(country="OUTDATED")


class Migration(migrations.Migration):
    dependencies = [
        ('wdi_website', '0069_province_iso3166_2_code'),
    ]

    operations = [migrations.RunPython(fix_nz_provinces), migrations.RunPython(delete_all_unclaimed_provinces)]
