import re

from django.db import migrations
from iso3166_2 import ISO3166_2

uk_2nd_level_subdivisions_mapping = {
    "City of London": "GB-ENG",
    "City of Edinburgh": "GB-SCT",
    "Antrim": "GB-NIR",
    "Ards": "GB-NIR",
    "Armagh": "GB-NIR",
    "Ballymena": "GB-NIR",
    "Ballymoney": "GB-NIR",
    "Banbridge": "GB-NIR",
    "Barnsley": "GB-ENG",
    "Bedfordshire": "GB-ENG",
    "Belfast": "GB-NIR",
    "Bournemouth": "GB-ENG",
    "Carrickfergus": "GB-NIR",
    "Castlereagh": "GB-NIR",
    "Cheshire": "GB-ENG",
    "City of Bristol": "GB-ENG",
    "City of Kingston upon Hull": "GB-ENG",
    "Coleraine": "GB-NIR",
    "Cookstown": "GB-NIR",
    "Craigavon": "GB-NIR",
    "Derry": "GB-NIR",
    "Down": "GB-ENG",
    "Dungannon": "GB-NIR",
    "Durham": "GB-ENG",
    "Eilean Siar (Western Isles)": "GB-SCT",
    "Fermanagh": "GB-NIR",
    "Larne": "GB-NIR",
    "Limavady": "GB-NIR",
    "Lisburn": "GB-NIR",
    "Magherafelt": "GB-NIR",
    "Moyle": "GB-NIR",
    "Newry and Mourne": "GB-NIR",
    "Newtownabbey": "GB-NIR",
    "North Down": "GB-NIR",
    "Northamptonshire": "GB-ENG",
    "Omagh": "GB-NIR",
    "Poole": "GB-ENG",
    "Shetland Islands": "GB-SCT",
    "Stockton-on-Tees": "GB-ENG",
    "Strabane": "GB-NIR",
    "The Scottish Borders": "GB-SCT",
    "The Vale of Glamorgan": "GB-WLS",
    "Warrington": "GB-ENG",
    "West Sussex": "GB-ENG",
    "Westminster": "GB-ENG",
    "Windsor and Maidenhead": "GB-ENG",
    "Wirral": "GB-ENG",
    "Wokingham": "GB-ENG",
    "Worcestershire": "GB-ENG",
    "Wrexham": "GB-WLS",
    "York": "GB-ENG",
}

iso = ISO3166_2()
iso_gb = iso["GB"]


def get_province_name(province_iso3166_2: dict):
    """
    Some provinces include extra information in their name - like province code or alternative spelling.
    This function removes these extra notes, leaving only one "canonical" name.
    """
    # Some Spanish subdivisions include stars in their names. They are pretty, but we don't want them.
    name = province_iso3166_2["name"].replace("*", "")

    # Checks for brackets and removes them if found
    if "[" not in name:
        return name

    # Describes a string that may or may not have a square brackets part.
    # If it does, real name is the part before brackets.
    # Example: Uppsala län [SE-03]
    name_without_square_brackets_regex = re.compile(r"(?P<name>[\w -']+\w)( \[.*\])?")
    match = name_without_square_brackets_regex.match(name)
    if not match:
        return None

    return match.group("name")


def get_ISO_subdivisions():
    second_subdivisions = []

    for subdivision in iso_gb:
        subdivision_dict = iso_gb[subdivision]
        if subdivision_dict["parentCode"] in ["GB-ENG", "GB-NIR", "GB-SCT", "GB-WLS"]:
            subdivision_name = get_province_name(subdivision_dict)
            second_subdivisions.append(subdivision_name)

    return second_subdivisions


def get_country_by_iso_subdivision_name(iso_name):
    for subdivision in iso_gb:
        subdivision_dict = iso_gb[subdivision]
        subdivision_normalised_name = get_province_name(subdivision_dict)
        if subdivision_normalised_name == iso_name:
            sd_parent = subdivision_dict["parentCode"]
            return sd_parent

    return None


def map_subdivisions_to_uk_countries(apps, schema_editor):
    Province = apps.get_model("wdi_website", "Province")
    Signature = apps.get_model("wdi_website", "Signature")

    # Only get old objects with no ISO code assigned
    iso_subdivisions = get_ISO_subdivisions()

    # Create new correct subdivisions for UK
    eng = Province(name="England", country="GB", iso3166_2_code="GB-ENG")
    nir = Province(name="Northern Ireland", country="GB", iso3166_2_code="GB-NIR")
    sct = Province(name="Scotland", country="GB", iso3166_2_code="GB-SCT")
    wls = Province(name="Wales", country="GB", iso3166_2_code="GB-WLS")
    eng.save()
    nir.save()
    sct.save()
    wls.save()
    uk_country_code_dict = {"GB-ENG": eng, "GB-NIR": nir, "GB-SCT": sct, "GB-WLS": wls}

    uk_signatures = Signature.objects.filter(country="GB").exclude(province=None).all()
    i = 0
    for s in uk_signatures:
        p = s.province
        db_province_name = p.name

        uk_country_code_for_province = None

        if db_province_name in iso_subdivisions:
            uk_country_code_for_province = get_country_by_iso_subdivision_name(
                db_province_name
            )

        elif db_province_name in uk_2nd_level_subdivisions_mapping:
            uk_country_code_for_province = uk_2nd_level_subdivisions_mapping[
                db_province_name
            ]

        else:
            raise Exception(f"Couldn't do anything with province {db_province_name}")

        containing_country = uk_country_code_dict[uk_country_code_for_province]
        s.province = containing_country
        s.save()
        i += 1

    print(f"Processed {i} signatures")


def delete_all_unclaimed_provinces(apps, schema_editor):
    Province = apps.get_model("wdi_website", "Province")

    # This will make sure that old provinces won't show up in form, while preserving the foreign key on Signature model
    # Only "unclaimed" subdivisions that have a default ISO code are affected
    Province.objects.filter(iso3166_2_code="-", country="GB").update(country="OUTDATED")


def unsync_gb_signatories(apps, schema_editor):
    Signature = apps.get_model("wdi_website", "Signature")
    signatures_to_update = Signature.objects.filter(
        country="GB", subscribe_to_emails=True
    ).exclude(province=None)
    signatures_to_update.update(synchronized_to_mailer=False)


class Migration(migrations.Migration):
    dependencies = [
        ("wdi_website", "0071_DATA_add_missing_provinces"),
    ]

    operations = [
        migrations.RunPython(map_subdivisions_to_uk_countries),
        migrations.RunPython(delete_all_unclaimed_provinces),
        migrations.RunPython(unsync_gb_signatories),
    ]
