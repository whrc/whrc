LANGUAGES = [
    ('en', "English"),
    ('es', "Spanish"),
    ('ru', "Russian"),
    ('de', "German"),
    ('pt', "Portuguese"),
    ('fr', "French"),
]